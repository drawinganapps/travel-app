import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TravelGroupCardWidget extends StatelessWidget {
  const TravelGroupCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final sideWidth = screenWidth * 0.04;
    return Container(
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.1), borderRadius: BorderRadius.circular(10)),
      width: 320,
      padding: const EdgeInsets.all(15),
      margin: EdgeInsets.only(left: sideWidth, right: sideWidth),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 15),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
            clipBehavior: Clip.antiAlias,
            child: Image.asset('assets/img/bromo.jpg',
                width: 60, height: 80, fit: BoxFit.fill),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text('Mount bromo volcano',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                  Text('Group members',
                      style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.star_rate, size: 30, color: Colors.orange),
                  const Icon(Icons.star_rate, size: 30, color: Colors.orange),
                  const Icon(Icons.star_rate, size: 30, color: Colors.orange),
                  const Icon(Icons.star_rate, size: 30, color: Colors.orange),
                  const Icon(Icons.star_rate, size: 30, color: Colors.grey),
                  Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: const Text('4.0',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20)),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
