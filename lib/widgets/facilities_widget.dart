import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:travel_app/models/facilities_model.dart';

class FacilitiesWidgets extends StatelessWidget {
  final FacilitiesModel facility;

  const FacilitiesWidgets({Key? key, required this.facility}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 15, right: 15),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
              border: Border.all(width: 2, color: Colors.grey.withOpacity(0.3))
            ),
            clipBehavior: Clip.antiAlias,
            margin: const EdgeInsets.only(bottom: 5),
            child: Image.asset('assets/icons/${facility.icon}',
                height: 50, width: 50, fit: BoxFit.cover),
          ),
          Text(facility.name,
              style: const TextStyle(
                  color: Colors.grey,
                  fontSize: 12,
                  fontWeight: FontWeight.w600))
        ],
      ),
    );
  }
}
