import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
            child: SizedBox(
          height: 50,
          child: TextField(
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(left: 15, right: 15),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide:
                        const BorderSide(color: Colors.grey, width: 0.5)),
                fillColor: Colors.grey.withOpacity(0.1),
                hintText: 'Search Destination',
                prefixIcon:
                    const Icon(Icons.search, color: Colors.black, size: 25),
                hintStyle: const TextStyle(
                    fontWeight: FontWeight.w200, color: Colors.black38)),
          ),
        )),
        Container(
          margin: const EdgeInsets.only(left: 30),
        ),
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: const Color.fromRGBO(250, 204, 0, 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: const Icon(Icons.tune, color: Colors.white, size: 30),
        ),
      ],
    );
  }
}
