import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/controller/filter_controller.dart';
import 'package:travel_app/models/items_filter.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final ItemsFilter filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      return Center(
        child: Container(
          margin: const EdgeInsets.only(left: 15, right: 15),
          height: 40,
          decoration: isSelected
              ? const BoxDecoration(
                  border:
                      Border(bottom: BorderSide(width: 3, color: Colors.red)))
              : const BoxDecoration(),
          child: Center(
            child: Text(filter.name,
                style: GoogleFonts.unna(
                    color: isSelected ? Colors.red : Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.bold)),
          ),
        ),
      );
    });
  }
}
