import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_app/models/place_model.dart';
import 'package:travel_app/routes/AppRoutes.dart';

class PlaceCardWidget extends StatelessWidget {
  final PlaceModel place;

  const PlaceCardWidget({Key? key, required this.place}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final sideWidth = screenWidth * 0.02;
    final marginWidth = screenWidth * 0.04;
    return GestureDetector(
      onTap: () {
        Get.toNamed(AppRoutes.DESTINATION_DETAIL, arguments: [place.id]);
      },
      child: Container(
        margin: EdgeInsets.only(left: marginWidth),
        width: 200,
        decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.1),
            borderRadius: BorderRadius.circular(10)),
        clipBehavior: Clip.antiAlias,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 200,
              width: 200,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/${place.cover}'),
                    fit: BoxFit.cover),
              ),
              margin: const EdgeInsets.only(bottom: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(
                            left: 10, right: 10, top: 5, bottom: 5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.yellow.withOpacity(0.5)),
                        child: Text('\$${place.price}',
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                      Container(
                        height: 25,
                        width: 25,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white.withOpacity(0.3)),
                        child: place.isFavorite
                            ? const Icon(Icons.favorite,
                                size: 18, color: Colors.pinkAccent)
                            : const Icon(Icons.favorite_border,
                                size: 18, color: Colors.white),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(place.name,
                          style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                    ],
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(Icons.place, color: Colors.grey.withOpacity(0.5)),
                      Text(place.location,
                          style: const TextStyle(color: Colors.grey))
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.only(
                        left: 10, right: 10, top: 5, bottom: 5),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.grey.withOpacity(0.2)),
                    child: Text(place.tripDaysLong,
                        style: TextStyle(color: Colors.grey.withOpacity(0.8))),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
