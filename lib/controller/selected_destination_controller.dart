import 'package:get/get.dart';
import 'package:travel_app/models/place_model.dart';

class SelectedDestinationController extends GetxController {
  var selectedDestination = null;
  void changeFilter(PlaceModel destination)  {
    selectedDestination = destination;
    update();
  }
}
