import 'package:get/get.dart';
import 'package:travel_app/controller/selected_destination_controller.dart';

class SelectedDestinationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SelectedDestinationController>(() => SelectedDestinationController());
  }
}
