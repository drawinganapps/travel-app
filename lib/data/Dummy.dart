import 'package:travel_app/models/facilities_model.dart';
import 'package:travel_app/models/items_filter.dart';
import 'package:travel_app/models/place_model.dart';

class Dummy {
  static List<ItemsFilter> filterList = <ItemsFilter>[
    const ItemsFilter('All', 'ALL'),
    const ItemsFilter('Latest', 'Latest'),
    const ItemsFilter('Popular', 'POPULAR'),
  ];

  static List<PlaceModel> places = [
    PlaceModel(0, 'Ubud', 'Bali', '2 Days', 79, 'ubud.jpg', true, 5.0),
    PlaceModel(1, 'Bromo', 'East Java', '2 Days', 19, 'bromo.jpg', false, 4.5),
    PlaceModel(2, 'Jogja', 'Jogja', '3 Days', 45, 'jogja.jpg', false, 5.0),
    PlaceModel(3, 'Ijen', 'Malang', '2 Days', 35, 'malang.jpg', false, 4.5),
  ];

  static List<FacilitiesModel> facilities = [
    FacilitiesModel('hotel-bed.png', 'Hotel'),
    FacilitiesModel('restaurant.png', 'Breakfast'),
    FacilitiesModel('explorer.png', 'Guide'),
    FacilitiesModel('car.png', 'Transport'),
    FacilitiesModel('take-a-photo.png', 'Doc'),
  ];

  static String content = 'Tur kami akan berlangsung di dalam dan di sekitar area yang kami kunjungi. Tur dimulai di kota Dharamsala dan berhenti di kota Bodhgaya, di mana kita akan bertemu dengan pemandu wisata. Kami akan menginap di rumah teman di Bodhgaya dan mengunjungi beberapa kuil, gereja, dan monumen.';
}
