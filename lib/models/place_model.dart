class PlaceModel {
  final int id;
  final String name;
  final String location;
  final String tripDaysLong;
  final int price;
  final String cover;
  final bool isFavorite;
  final double ratings;

  PlaceModel(this.id, this.name, this.location, this.tripDaysLong, this.price,
      this.cover, this.isFavorite, this.ratings);
}
