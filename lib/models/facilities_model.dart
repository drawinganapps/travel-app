class FacilitiesModel {
  final String icon;
  final String name;

  FacilitiesModel(this.icon, this.name);
}