import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:travel_app/data/Dummy.dart';
import 'package:travel_app/models/facilities_model.dart';
import 'package:travel_app/models/place_model.dart';
import 'package:travel_app/routes/AppRoutes.dart';
import 'package:travel_app/widgets/facilities_widget.dart';

class DestinationDetailScreen extends StatelessWidget {
  const DestinationDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final sideWidth = screenWidth * 0.04;
    int id = Get.arguments[0];
    final List<FacilitiesModel> facilities = Dummy.facilities;
    final PlaceModel destination =
        Dummy.places.firstWhere((element) => element.id == id);

    return Scaffold(
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          Container(
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.grey.withOpacity(0.2),
                  ),
                  padding: const EdgeInsets.all(2),
                  child: IconButton(
                    onPressed: () {
                      Get.toNamed(AppRoutes.DASHBOARD);
                    },
                    icon: const Icon(Icons.arrow_back_ios_outlined),
                  ),
                ),
                Text(destination.name,
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold)),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15)),
                  child: IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.menu),
                    iconSize: 35,
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
            child: Container(
              height: screenWidth * 0.92,
              margin: const EdgeInsets.only(top: 15, bottom: 15),
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/img/${destination.cover}'),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(15)),
              clipBehavior: Clip.antiAlias,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Flexible(
                      child: Text(destination.name,
                          style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Colors.white))),
                  Container(
                    padding: const EdgeInsets.only(
                        left: 5, right: 5, top: 5, bottom: 5),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.grey.withOpacity(0.5)),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.place,
                          color: Colors.white,
                          size: 15,
                        ),
                        Text(destination.location,
                            style: const TextStyle(color: Colors.white)),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey.withOpacity(0.1)),
                          padding: const EdgeInsets.all(15),
                          child: const Icon(Icons.watch_later,
                              color: Color.fromRGBO(250, 204, 0, 1), size: 30),
                        )
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(bottom: 5),
                            child: const Text('Trip Duration',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w600)),
                          ),
                          Text(destination.tripDaysLong,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w900,
                                  fontSize: 20)),
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.grey.withOpacity(0.1)),
                          padding: const EdgeInsets.all(15),
                          child: const Icon(Icons.star_rate,
                              color: Color.fromRGBO(250, 204, 0, 1), size: 30),
                        )
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(bottom: 5),
                            child: const Text('Ratings',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w600)),
                          ),
                          Text(destination.ratings.toString(),
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w900,
                                  fontSize: 20)),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            child: Text(Dummy.content,
                style: const TextStyle(fontSize: 15, color: Colors.grey),
                textAlign: TextAlign.justify),
          ),
          Container(
            padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
            margin: const EdgeInsets.only(top: 15, bottom: 15),
            child: const Text('Packages Included:',
                style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.w900),
                textAlign: TextAlign.justify),
          ),
          SizedBox(
            height: 80,
            child: ListView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              children: List.generate(facilities.length, (index) {
                return FacilitiesWidgets(facility: facilities[index]);
              }),
            ),
          )
        ],
      ),
      bottomNavigationBar: Container(
        height: 100,
        padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
        child: Row(
          children: [
            Container(
              height: 65,
              margin: const EdgeInsets.only(right: 20),
              width: screenWidth * 0.3,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(250, 204, 0, 1),
                borderRadius: BorderRadius.circular(15),
              ),
              child: TextButton(
                child: Text('\$${destination.price}',
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
                onPressed: () {},
              ),
            ),
            Expanded(
              child: Container(
                height: 65,
                width: screenWidth * 0.3,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: TextButton(
                  child: const Text('Book Now',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold)),
                  onPressed: () {},
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
