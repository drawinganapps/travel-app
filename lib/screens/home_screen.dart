import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:travel_app/controller/filter_controller.dart';
import 'package:travel_app/data/Dummy.dart';
import 'package:travel_app/models/items_filter.dart';
import 'package:travel_app/models/place_model.dart';
import 'package:travel_app/widgets/filter_widget.dart';
import 'package:travel_app/widgets/place_card_widget.dart';
import 'package:travel_app/widgets/search_widget.dart';
import 'package:travel_app/widgets/tarvel_groups_card_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      final screenWidth = MediaQuery.of(context).size.width;
      final screenHeight = MediaQuery.of(context).size.height;
      final sideWidth = screenWidth * 0.04;
      final List<ItemsFilter> filterList = Dummy.filterList;
      final List<PlaceModel> places = Dummy.places;
      return Scaffold(
          body: ListView(
            physics: const BouncingScrollPhysics(),
            children: [
              Container(
                padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      child: IconButton(
                        onPressed: () {},
                        icon: const Icon(Icons.menu),
                        iconSize: 35,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(right: 20),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50)),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset('assets/img/man.png',
                          width: 50, height: 50, fit: BoxFit.cover),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
                margin: EdgeInsets.only(
                    top: screenHeight * 0.03, bottom: screenHeight * 0.025),
                child: Text('Discover Indonesia!',
                    style: GoogleFonts.unna(
                      fontSize: 40,
                      letterSpacing: 0.5,
                      fontWeight: FontWeight.w900,
                    )),
              ),
              Container(
                padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
                child: const SearchWidget(),
              ),
              Container(
                padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
                margin: EdgeInsets.only(
                    top: screenHeight * 0.03, bottom: screenHeight * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Travel Places',
                        style: GoogleFonts.unna(
                            fontSize: 23, fontWeight: FontWeight.bold)),
                    Row(
                      children: [
                        Text('Show more',
                            style: GoogleFonts.unna(
                                color: Colors.grey.withOpacity(0.8),
                                fontSize: 15)),
                        Icon(
                          Icons.chevron_right,
                          color: Colors.grey.withOpacity(0.5),
                        )
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 35,
                child: ListView(
                    physics: const BouncingScrollPhysics(),
                    padding: const EdgeInsets.all(0),
                    scrollDirection: Axis.horizontal,
                    children: List.generate(filterList.length, (index) {
                      return GestureDetector(
                        onTap: () {
                          controller.changeFilter(index);
                        },
                        child: FilterCategoriesWidget(
                            isSelected: index == controller.selectedFilter,
                            filter: filterList[index]),
                      );
                    })),
              ),
              Container(
                height: 250,
                margin: const EdgeInsets.only(top: 15),
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(places.length, (index) {
                    return PlaceCardWidget(place: places[index]);
                  }),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: sideWidth, right: sideWidth),
                margin: EdgeInsets.only(
                    top: screenHeight * 0.03, bottom: screenHeight * 0.03),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Travel Groups',
                        style: GoogleFonts.unna(
                            fontSize: 23, fontWeight: FontWeight.bold)),
                    Row(
                      children: [
                        Text('Show more',
                            style: GoogleFonts.unna(
                                color: Colors.grey.withOpacity(0.8),
                                fontSize: 15)),
                        Icon(
                          Icons.chevron_right,
                          color: Colors.grey.withOpacity(0.5),
                        )
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 100,
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(places.length, (index) {
                    return const TravelGroupCardWidget();
                  }),
                ),
              ),
            ],
          ),
          bottomNavigationBar: SizedBox(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.home_rounded,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
                Container(
                  height: 55,
                  width: 55,
                  decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(18)),
                  child: IconButton(
                    enableFeedback: false,
                    onPressed: () {},
                    icon: const Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 30,
                    ),
                  ),
                ),
                IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.chat_bubble,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
              ],
            ),
          ));
    });
  }
}
