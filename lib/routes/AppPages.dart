import 'package:get/get.dart';
import 'package:travel_app/controller/filter_binding.dart';
import 'package:travel_app/controller/selected_destination_binding.dart';
import 'package:travel_app/screens/destination_detail_screen.dart';
import 'package:travel_app/screens/home_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreen(),
        bindings: [FilterBinding(), SelectedDestinationBinding()],
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.DESTINATION_DETAIL,
        page: () => const DestinationDetailScreen(),
        binding: SelectedDestinationBinding(),
        transition: Transition.rightToLeft
    ),
  ];
}
